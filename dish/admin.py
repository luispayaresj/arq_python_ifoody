from django.contrib import admin

# Register your models here.
from dish.models import Dish

admin.site.register(Dish)
