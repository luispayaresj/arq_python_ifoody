from django.db import models


# Create your models here.

class Dish(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    type = models.CharField(max_length=100, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    price = models.DecimalField(blank=False, null=False,decimal_places=3, max_digits=10)
    is_available = models.BooleanField(default=True)
    stock = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return self.name
