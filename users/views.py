from django.shortcuts import render

def home(request):
    """
    You can show the index view in your proyect
    """
    return render(request, 'base.html')