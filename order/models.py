from django.db import models
from users.models import User
from dish.models import Dish


# Create your models here.

class Order(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)
    id_dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    date = models.DateTimeField()
    total = models.DecimalField(blank=False, null=False, decimal_places=3, max_digits=10)
    num_request = models.IntegerField(default=0)
    def __str__(self):
        return f'{self.id_user}, {self.id_dish}, {self.num_request}'
