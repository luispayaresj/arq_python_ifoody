from django.db import models


# Create your models here.

class Bill(models.Model):
    num_request = models.IntegerField(default=0)
    date = models.DateTimeField(blank=True, null=True)
    subtotal = models.DecimalField(blank=True, null=True, decimal_places=3, max_digits=10)
    total = models.DecimalField(blank=True, null=True, decimal_places=3, max_digits=10)

    def __str__(self):
        return f'Bill #{self.id}'
