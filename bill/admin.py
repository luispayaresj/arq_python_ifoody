from django.contrib import admin

# Register your models here.
from bill.models import Bill

admin.site.register(Bill)